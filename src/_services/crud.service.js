import { fetchWrapper } from '../_helpers';

const baseUrl = '/';

export const crudService =  {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

function getAll(url) {
    return fetchWrapper.get(baseUrl + url);
}

function getById(id, url) {
    return fetchWrapper.get(`${baseUrl + url}/${id}`);
}

function create(params, url) {
    return fetchWrapper.post(baseUrl + url, params);
}

function update(id, params, url) {
    return fetchWrapper.put(`${baseUrl + url}/${id}`, params);
}

// prefixed with underscored because delete is a reserved word in javascript
function _delete(id, url) {
    return fetchWrapper.delete(`${baseUrl + url}/${id}`);
}
