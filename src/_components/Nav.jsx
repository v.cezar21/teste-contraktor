import React from 'react';
import { NavLink } from 'react-router-dom';

function Nav() {
    return (
        <nav className="navbar navbar-expand  navbar-light"  style={{backgroundColor: "#e3f2fd"}}>
            <div className="navbar-nav">
                <NavLink exact to="/" className="nav-item nav-link">Home</NavLink>
                <NavLink to="/parts" className="nav-item nav-link">Partes</NavLink>
                <NavLink to="/contracts" className="nav-item nav-link">Contratos</NavLink>
            </div>
        </nav>
    );
}

export { Nav }; 