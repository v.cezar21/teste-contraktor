

export function configureFakeBackend() {
    // array in local storage for user records
    JSON.parse(localStorage.getItem('parts')) || localStorage.setItem('parts', JSON.stringify([{ 
        id: 1,
        firstName: 'João',
        lastName: 'da Silva',
        email: 'joao.silva@contraktor.com.br',
        cpf: '208.498.930-20',
        tel: '31989308060',
    }]))

    let parts = JSON.parse(localStorage.getItem('parts'));

    JSON.parse(localStorage.getItem('contracts')) || localStorage.setItem('contracts', JSON.stringify([{ 
        id: 1,
        title: 'primeiro contrato',
        dtIni: '2020-01-01',
        dtVenc: '2020-12-31',
        part:[{value: 1, label: "João da Silva - 208.498.930-20"}]
    }]))

    let contracts =  JSON.parse(localStorage.getItem('contracts'))


    // monkey patch fetch to setup fake backend
    let realFetch = window.fetch;
    window.fetch = function (url, opts) {
        return new Promise((resolve, reject) => {
            // wrap in timeout to simulate server api call
            setTimeout(handleRoute, 500);

            function handleRoute() {
                const { method } = opts;
                switch (true) {
                    case url.endsWith('/parts') && method === 'GET':
                        return getParts();
                    case url.match(/\/parts\/\d+$/) && method === 'GET':
                        return getPartById();
                    case url.endsWith('/parts') && method === 'POST':
                        return createPart();
                    case url.match(/\/parts\/\d+$/) && method === 'PUT':
                        return updatePart();
                    case url.match(/\/parts\/\d+$/) && method === 'DELETE':
                        return deletePart();
                    case url.endsWith('/contracts') && method === 'GET':
                        return getContracts();
                    case url.match(/\/contracts\/\d+$/) && method === 'GET':
                        return getContractById();
                    case url.endsWith('/contracts') && method === 'POST':
                        return createContract();
                    case url.match(/\/contracts\/\d+$/) && method === 'PUT':
                        return updateContract();
                    case url.match(/\/contracts\/\d+$/) && method === 'DELETE':
                        return deleteContract();
                    default:
                        return realFetch(url, opts)
                            .then(response => resolve(response))
                            .catch(error => reject(error));
                }
            }


            function getParts() {
                return ok(parts);
            }

            function getPartById() {
                let part = parts.find(x => x.id === idFromUrl());
                return ok(part);
            }
    
            function createPart() {
                const part = body();

                if (parts.find(x => x.email === part.email)) {
                    return error(`Uma parte com esse Email: ${part.email} já existe`);
                }
                if (parts.find(x => x.cpf === part.cpf)) {
                    return error(`Uma parte com esse Cpf: ${part.cpf} já existe`);
                }

                
                part.id = newId(parts);
                part.dateCreated = new Date().toISOString();
                parts.push(part);
                localStorage.setItem('parts', JSON.stringify(parts));

                return ok();
            }
    
            function updatePart() {
                let params = body();
                let part = parts.find(x => x.id === idFromUrl());

            
                Object.assign(part, params);
                localStorage.setItem('parts', JSON.stringify(parts));

                return ok();
            }
    
            function deletePart() {
                parts = parts.filter(x => x.id !== idFromUrl());
                localStorage.setItem('parts', JSON.stringify(parts));

                return ok();
            }





            function getContracts() {
                return ok(contracts);
            }

            function getContractById() {
                let contract = contracts.find(x => x.id === idFromUrl());
                return ok(contract);
            }
    
            function createContract() {
                const contract = body();
                
                contract.id = newId(contracts);
                contract.dateCreated = new Date().toISOString();
                contracts.push(contract);
                localStorage.setItem('contracts', JSON.stringify(contracts));

                return ok();
            }
    
            function updateContract() {
                let params = body();
                let contract = contracts.find(x => x.id === idFromUrl());

            
                Object.assign(contract, params);
                localStorage.setItem('contracts', JSON.stringify(contracts));

                return ok();
            }
    
            function deleteContract() {
                contracts = contracts.filter(x => x.id !== idFromUrl());
                localStorage.setItem('contracts', JSON.stringify(contracts));

                return ok();
            }
            
            
            // helper functions

            function ok(body) {
                resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(body)) });
            }

            function error(message) {
                resolve({ status: 400, text: () => Promise.resolve(JSON.stringify({ message })) });
            }

            function idFromUrl() {
                const urlC = url.split('/');
                return parseInt(urlC[urlC.length - 1]);
            }

            function body() {
                return opts.body && JSON.parse(opts.body);    
            }

            function newId(model) {
                return model.length ? Math.max(...model.map(x => x.id)) + 1 : 1;
            }
            
        });
    }
}