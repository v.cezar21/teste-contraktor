import React from 'react';
import { Link } from 'react-router-dom';

function Home() {
    return (
        <div>
            <h1>Contraktor</h1>
            <p>Aqui é possivel cadastrar, editar, ler e excluir contratos e partes, assim como relaciona-los</p>
            <p><Link to="contracts"> Gerenciar Contratos</Link></p>
            <p><Link to="parts"> Gerenciar Partes</Link></p>
        </div>
    );
}

export { Home };