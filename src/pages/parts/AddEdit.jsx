import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { crudService, alertService } from '../../_services';

function AddEdit({ history, match }) {
    const { id } = match.params;
    const isAddMode = !id;
    
    const initialValues = {
        firstName: '',
        lastName: '',
        email: '',
        cpf: '',
        tel: ''
    };

    const validationSchema = Yup.object().shape({
        firstName: Yup.string()
            .required('Nome é obrigatório'),
        lastName: Yup.string()
            .required('Sobrenome é obrigatório'),
        email: Yup.string()
            .email('Formato do email está inválido')
            .required('Email é Obrigatório'),
        cpf: Yup.string()
            .required('CPF é obrigatório'),
        tel: Yup.string()
            .required('Telefone é obrigatório'),
       
    });

    function onSubmit(fields, { setStatus, setSubmitting }) {
        setStatus();
        if (isAddMode) {
            createPart(fields, setSubmitting);
        } else {
            updatePart(id, fields, setSubmitting);
        }
    }

    function createPart(fields, setSubmitting) {
        crudService.create(fields, '/parts')
            .then(() => {
                alertService.success('Parte criada com sucesso', { keepAfterRouteChange: true });
                history.push('.');
            })
            .catch(error => {
                setSubmitting(false);
                alertService.error(error);
            });
    }

    function updatePart(id, fields, setSubmitting) {
        crudService.update(id, fields, '/parts')
            .then(() => {
                alertService.success('Parte atualizada com sucesso', { keepAfterRouteChange: true });
                history.push('..');
            })
            .catch(error => {
                setSubmitting(false);
                alertService.error(error);
            });
    }

    return (
        <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
            {({ errors, touched, isSubmitting, setFieldValue }) => {
                const [part, setPart] = useState({});

                useEffect(() => {
                    if (!isAddMode) {
                        // get user and set form fields
                        crudService.getById(id,'/parts').then(part => {
                            const fields = ['firstName', 'lastName', 'email', 'cpf', 'tel'];
                            fields.forEach(field => setFieldValue(field, part[field], false));
                            setPart(part);
                        });
                    }
                }, []);

                return (
                    <Form>
                        <h1>{isAddMode ? 'Adicionar' : 'Editar'}</h1>
                        <div className="form-row">
                        
                            <div className="form-group col-5">
                                <label>Nome</label>
                                <Field name="firstName" type="text" className={'form-control' + (errors.firstName && touched.firstName ? ' is-invalid' : '')} />
                                <ErrorMessage name="firstName" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group col-5">
                                <label>Sobrenome</label>
                                <Field name="lastName" type="text" className={'form-control' + (errors.lastName && touched.lastName ? ' is-invalid' : '')} />
                                <ErrorMessage name="lastName" component="div" className="invalid-feedback" />
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col-7">
                                <label>Email</label>
                                <Field name="email" type="text" className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')} />
                                <ErrorMessage name="email" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group col-7">
                                <label>Cpf</label>
                                <Field name="cpf" type="text" className={'form-control' + (errors.cpf && touched.cpf ? ' is-invalid' : '')} />
                                <ErrorMessage name="cpf" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group col-7">
                                <label>Telefone</label>
                                <Field name="tel" type="text" className={'form-control' + (errors.tel && touched.tel ? ' is-invalid' : '')} />
                                <ErrorMessage name="tel" component="div" className="invalid-feedback" />
                            </div>
                            
                        </div>
                      
                        
                        <div className="form-group">
                            <button type="submit" disabled={isSubmitting} className="btn btn-primary">
                                {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                Salvar
                            </button>
                            <Link to={isAddMode ? '.' : '..'} className="btn btn-link">Cancelar</Link>
                        </div>
                    </Form>
                );
            }}
        </Formik>
    );
}

export { AddEdit };