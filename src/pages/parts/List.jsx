import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import { crudService } from '../../_services';

function List({ match }) {
    const { path } = match;
    const [parts, setParts] = useState(null);

    useEffect(() => {
        crudService.getAll('/parts').then(x => setParts(x));
    }, []);

    function  deletePart(id) {
      
        crudService.getAll('/contracts').then(x => 
            {
            let res = false
               x.map((contract) => {
                    contract.part.map((p) => {
                        if(p.value == id)
                        {
                            res =  contract.id
                        }
                    })
                })
              
                if(res == false){
                    setParts(parts.map(x => {
                        if (x.id === id) { x.isDeleting = true; }
                        return x;
                    }));
                    crudService.delete(id, '/parts').then(() => {
                        setParts(parts => parts.filter(x => x.id !== id));
                    });
                }
                else{
                    alert(`Não é possível deletar, ela está vinculada ao contrato ${res}, desvincule para continuar!`)
                }
            })

    }

    return (
        <div>
            <h1>Partes</h1>
            <Link to={`${path}/add`} className="btn btn-sm btn-primary mb-2">Adicionar</Link>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th style={{ width: '10%' }}>Id</th>
                        <th style={{ width: '30%' }}>Nome</th>
                        <th style={{ width: '30%' }}>Email</th>
                        <th style={{ width: '30%' }}>Cpf</th>
                        <th style={{ width: '10%' }}></th>
                    </tr>
                </thead>
                <tbody>
                    {parts && parts.map(part =>
                        <tr key={part.id}>
                            <td> {part.id} </td>
                            <td> {part.firstName} {part.lastName}</td>
                            <td>{part.email}</td>
                            <td>{part.cpf}</td>
                            <td style={{ whiteSpace: 'nowrap' }}>
                                <Link to={`${path}/edit/${part.id}`} className="btn btn-sm btn-info mr-1">Editar</Link>
                                <button onClick={() =>  {if(window.confirm('Tem certeza que deseja deletar essa parte?')){deletePart(part.id)}}} className="btn btn-sm btn-danger btn-delete-part" disabled={part.isDeleting}>
                                    {part.isDeleting 
                                        ? <span className="spinner-border spinner-border-sm"></span>
                                        : <span>Excluir</span>
                                    }
                                </button>
                            </td>
                        </tr>
                    )}
                    {!parts &&
                        <tr>
                            <td colSpan="4" className="text-center">
                                <div className="spinner-border spinner-border-lg align-center"></div>
                            </td>
                        </tr>
                    }
                    {parts && !parts.length &&
                        <tr>
                            <td colSpan="4" className="text-center">
                                <div className="p-2">Sem dados para exibir</div>
                            </td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
    );
}

export { List };