import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import Select from "react-select";
import * as Yup from 'yup';

import { crudService, alertService } from '../../_services';

function AddEdit({ history, match }) {
    const { id } = match.params;
    const isAddMode = !id;
    
    const initialValues = {
        title: '',
        dtIni: '',
        dtVenc: '',
        part: "",
        
        
    };
    const [parts, setParts] = useState([])
    const [partSelect, setSelectPart] = useState([])



    const validationSchema = Yup.object().shape({
        title: Yup.string()
            .required('Titulo é obrigatório'),
        dtIni: Yup.string()
            .required('Data de inicio é obrigatório'),
        dtVenc: Yup.string()
            .required('Data de vencimento é obrigatorio'),

            
    });


    const loadParts = (edit = false, contract = []) =>{

        crudService.getAll('/parts').then(allParts => {
          
            if(edit)
            {

                allParts.map((v) => {
                    if(contract && contract.part && contract.part.length > 0)
                    {
                        contract.part.map((x) => {
                            if(x.value == v.id){
                                setSelectPart(partSelect=> [...partSelect, {
                                    value: v.id,
                                    label: `${v.firstName} ${v.lastName} - ${v.cpf}`
                                }])
                                
                            }
                        })
                    }
                 
                   
                })
                
                allParts.filter(f => contract.part.map(x=> x.value).indexOf(f.id) == -1).map(v => {
                    setParts(parts => [...parts,{
                        value: v.id,
                        label: `${v.firstName} ${v.lastName} - ${v.cpf}`
                    }])
                })
                

            }
            else{
                allParts.map((e) => {
                    setParts(parts=> [...parts,{
                        value: e.id,
                        label: `${e.firstName} ${e.lastName} - ${e.cpf}`
                    }])
                })
            }
        
        });


      

        
    }

    function onSubmit(fields, { setStatus, setSubmitting }) {
        setStatus();
        if (isAddMode) {
            createContract(fields, setSubmitting);
        } else {
            updateContract(id, fields, setSubmitting);
        }
    }

    

    function createContract(fields, setSubmitting) {
        crudService.create(fields, '/contracts')
            .then(() => {
                alertService.success('Contrato criado com sucesso', { keepAfterRouteChange: true });
                history.push('.');
            })
            .catch(error => {
                setSubmitting(false);
                alertService.error(error);
            });
    }

    function updateContract(id, fields, setSubmitting) {
        crudService.update(id, fields, '/contracts')
            .then(() => {
                alertService.success('Contrato atualizado com sucesso', { keepAfterRouteChange: true });
                history.push('..');
            })
            .catch(error => {
                setSubmitting(false);
                alertService.error(error);
            });
    }

    

    return (
        <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
            {({ errors, touched, isSubmitting, setFieldValue,  handleChange, handleBlur, values }) => {
                const [contract, setContracts] = useState({});

                useEffect(() => {

                    
                    if (!isAddMode) {
                        
                        crudService.getById(id, '/contracts').then(contract => {

                            const fields = ['title', 'dtIni', 'dtVenc', 'part'];
                            fields.forEach(field => setFieldValue(field, contract[field], false));
                           
                            loadParts(true, contract)
                            setContracts(contract);
                        });
                    }
                    else{
                        loadParts(false)
                    }

                }, []);

                return (
                    <Form>
                        <h1>{isAddMode ? 'Adicionar' : 'Editar'}</h1>
                        <div className="form-row">
                        
                            <div className="form-group col-5">
                                <label>Title</label>
                                <Field name="title" type="text" className={'form-control' + (errors.title && touched.title ? ' is-invalid' : '')} />
                                <ErrorMessage name="title" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group col-5">
                                <label>Data de inicio do contrato</label>
                                <Field name="dtIni" type="date" className={'form-control' + (errors.dtIni && touched.dtIni ? ' is-invalid' : '')} />
                                <ErrorMessage name="dtIni" component="div" className="invalid-feedback" />
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col-7">
                                <label>Data de vencimento do contrato</label>
                                <Field name="dtVenc" type="date" className={'form-control' + (errors.dtVenc && touched.dtVenc ? ' is-invalid' : '')} />
                                <ErrorMessage name="dtVenc" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group col-7">
                                <label>Partes</label>
                                <Select
                                        placeholder="Partes do Contrato"
                                        onChange={selectedOption => {
                                            setSelectPart(selectedOption)
                                            values.part = selectedOption
                                        }}
                                        isMulti
                                        isSearchable={true}
                                        options={parts}
                                        value={partSelect}
                                        name="part"
                                        isLoading={false}
                                        loadingMessage={() => "Carregando Partes do contrato"}
                                        noOptionsMessage={() => "Todas as partes disponiveis já foram adicionadas"}
                                        />
                                <ErrorMessage name="parciais" component="div" className="invalid-feedback" />
                            </div>
                        </div>
                      
                        
                        <div className="form-group">
                            <button type="submit" disabled={isSubmitting} className="btn btn-primary">
                                {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                Salvar
                            </button>
                            <Link to={isAddMode ? '.' : '..'} className="btn btn-link">Cancelar</Link>
                        </div>
                    </Form>
                );
            }}
        </Formik>
    );
}

export { AddEdit };