import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import { crudService } from '../../_services';

function List({ match }) {
    const { path } = match;
    const [contracts, setContracts] = useState(null);

    useEffect(() => {
        crudService.getAll('/contracts').then(x => setContracts(x));
    }, []);

    function deleteContract(id) {
        setContracts(contracts.map(x => {
            if (x.id === id) { x.isDeleting = true; }
            return x;
        }));
        crudService.delete(id, '/contracts').then(() => {
            setContracts(contracts => contracts.filter(x => x.id !== id));
        });
    }

    return (
        <div>
            <h1>Contratos</h1>
            <Link to={`${path}/add`} className="btn btn-sm btn-primary mb-2">Adicionar</Link>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th style={{ width: '30%' }}>Título</th>
                        <th style={{ width: '30%' }}>Dt. Inicio</th>
                        <th style={{ width: '30%' }}>Dt. Vencimento</th>
                        <th style={{ width: '10%' }}></th>
                    </tr>
                </thead>
                <tbody>
                    {contracts && contracts.map(contract =>
                        <tr key={contract.id}>
                            <td>{contract.title} </td>
                            <td>{contract.dtIni}</td>
                            <td>{contract.dtVenc}</td>
                            <td style={{ whiteSpace: 'nowrap' }}>
                                <Link to={`${path}/edit/${contract.id}`} className="btn btn-sm btn-info mr-1">Editar</Link>
                                <button onClick={() => deleteContract(contract.id)} className="btn btn-sm btn-danger btn-delete" disabled={contract.isDeleting}>
                                    {contract.isDeleting 
                                        ? <span className="spinner-border spinner-border-sm"></span>
                                        : <span>Excluir</span>
                                    }
                                </button>
                            </td>
                        </tr>
                    )}
                    {!contracts &&
                        <tr>
                            <td colSpan="4" className="text-center">
                                <div className="spinner-border spinner-border-lg align-center"></div>
                            </td>
                        </tr>
                    }
                    {contracts && !contracts.length &&
                        <tr>
                            <td colSpan="4" className="text-center">
                                <div className="p-2">Sem contratos para exibir</div>
                            </td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
    );
}

export { List };