# Teste crud Contraktor

 - Esse projeto foi criado com  [Create React App](https://github.com/facebook/create-react-app).
 - [Bootstrap](https://getbootstrap.com/)
 - [WebPack](https://webpack.js.org/)
 - Plugins (react router dom, yup, webpack, formik)


## Scripts

Clone o repositório:
## SSH:
### `git clone git@gitlab.com:v.cezar21/teste-contraktor.git`
## ou HTTPS:
### `git clone https://gitlab.com/v.cezar21/teste-contraktor.git`


No diretorio no projeto, execute:
### `npm install && npm start`


O app estará acessivel em modo de desenvolvimento.\
Abra [http://localhost:8080](http://localhost:8080) para visualizar em seu navegador.



